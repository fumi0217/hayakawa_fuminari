

(function($){
	  $.isBlank = function(obj){
	    return(!obj || $.trim(obj) === "");
	  };
})(jQuery);

$(document).ready(function(){

	$(function(){
		$(":input").focus(function(){
			$(this).css("background", "#DFEEFF");
		}).blur(function(){
			$(this).css("background", "");
		});
	});

	$(".commentBtn").prop("disabled", true);

	function checkErrors(commentArea){
		var isError = false;
		commentArea.siblings("#commentBtn").prop("disabled", false);
		if(commentArea.siblings(".error").text() != ""){
			isError = true;
		}
		if(isError){
			commentArea.siblings("#commentBtn").prop("disabled", true);
		}
	}


	$(".comment").each(function(){
		$(this).blur(function(){
			var value = $(this).val();
			$(this).siblings(".error").text("").css('color', 'red');
			if($.isBlank(value)){
				$(this).siblings(".error").text("コメントの本文を入力してください").css('color', 'red');
			}else if(value.length > 500){
				$(this).siblings(".error").text("本文は500文字以下で入力してください").css('color', 'red');
			}
			checkErrors($(this));
		});
	});
});