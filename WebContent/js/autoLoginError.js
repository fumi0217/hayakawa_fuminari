(function($){
	  $.isBlank = function(obj){
	    return(!obj || $.trim(obj) === "");
	  };
})(jQuery);

$(document).ready(function(){

	$(function(){
		$(":input").focus(function(){
			$(this).css("background", "#DFEEFF");
		}).blur(function(){
			$(this).css("background", "");
		});
	});

	$("#loginBtn").prop("disabled", true);
	if($.isBlank($(".errorMessages").text())){
		$(".errorMessages").text("ログインIDとパスワードを入力してください").css('color', 'red');
	}

	function checkErrors(){
		if(!$.isBlank($("#login-id").val()) && !$.isBlank($("#password").val())){
			$("#loginBtn").prop("disabled", false);
			$(".errorMessages").text("").css('color', 'red');
		}else{
			$("#loginBtn").prop("disabled", true);
			$(".errorMessages").text("ログインIDとパスワードを入力してください").css('color', 'red');
		}
	}

	$("#login-id").blur(function(){
		checkErrors();
	});

	$("#password").blur(function(){
		checkErrors();
	});
});