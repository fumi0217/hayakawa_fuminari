(function($){
	  $.isBlank = function(obj){
	    return(!obj || $.trim(obj) === "");
	  };
})(jQuery);


$(document).ready(function(){
	var titleInput = $("#titleForm #title");
	var textInput = $("#textForm #text");
	var categoryInput = $("#categoryForm #category");
	var inputs = new Array(titleInput, textInput, categoryInput);

	$(function(){
		$(":input").focus(function(){
			$(this).css("background", "#DFEEFF");
		}).blur(function(){
			$(this).css("background", "");
		});
	});


	titleInput.siblings(".error").text("タイトルを入力してください").css('color', 'red');
	categoryInput.siblings(".error").text("カテゴリーを入力してください").css('color', 'red');
	textInput.siblings(".error").text("本文を入力してください").css('color', 'red');

	$("#newMessageBtn").prop("disabled", true);


	function checkErrors(){
		var isError = false;
		$("#newMessageBtn").prop("disabled", false);
		$(".error").each(function(){
			if($(this).text() != ""){
				isError = true;
			}
		});
		if(isError){
			$("#newMessageBtn").prop("disabled", true);
		}
	}


	titleInput.blur(function(){
		var value = titleInput.val();
		titleInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(value)){
			titleInput.siblings(".error").text("タイトルを入力してください").css('color', 'red');
		}else if(value.length > 30){
			titleInput.siblings(".error").text("タイトルは30文字以下で入力してください").css('color', 'red');
		}
		checkErrors();
	});

	categoryInput.blur(function(){
		var value = categoryInput.val();
		categoryInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(value)){
			categoryInput.siblings(".error").text("カテゴリーを入力してください").css('color', 'red');
		}else if(value.length > 10){
			element.siblings(".error").text("カテゴリーは10文字以下で入力してください").css('color', 'red');
		}
		checkErrors();
	});

	textInput.blur(function(){
		var value = textInput.val();
		textInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(value)){
			textInput.siblings(".error").text("本文を入力してください").css('color', 'red');
		}else if(value.length > 1000){
			element.siblings(".error").text("本文は1000文字以下で入力してください").css('color', 'red');
		}
		checkErrors();
	});
});

