(function($){
	  $.isBlank = function(obj){
	    return(!obj || $.trim(obj) === "");
	  };
})(jQuery);


$(document).ready(function(){
	var loginIdInput = $("#loginIdForm #login_id");
	var passwordInput = $("#passwordForm #password");
	var secondPasswordInput = $("#secondPasswordForm #second_password");
	var nameInput = $("#nameForm #name");
	var branchInput = $("#branchForm #branch");
	var positionInput = $("#positionForm #position");

	$(function(){
		$(":input").focus(function(){
			$(this).css("background", "#DFEEFF");
		}).blur(function(){
			$(this).css("background", "");
		});
	});

	function checkErrors(){
		var isError = false;
		$("#settingBtn").prop("disabled", false);
		$(".error").each(function(){
			if($(this).text() != ""){
				isError = true;
			}
		});
		if(isError){
			$("#settingBtn").prop("disabled", true);
		}
	}

	$(function(){
		$(":input").change(function(){
			var isError = false;
			$("#settingBtn").prop("disabled", false);
			$(".error").each(function(){
				if($(this).text() != ""){
					isError = true;
				}
			});
			if(isError){
				$("#settingBtn").prop("disabled", true);
			}
		});
	});
	$(function(){
		$(":input").change(function(){
			var errorMessages = [];
			$(".error").each(function(){
				if(!$.isBlank($(this).text())){
					errorMessages.push($(this).text);
				}
			});
			if(errorMessages.length === 0){
				$("#settingBtn").prop("disabled", false);
			}else{
				$("#settingBtn").prop("disabled", true);
			}
		});
	});



	loginIdInput.blur(function(){
		var error = "";
		var value = loginIdInput.val();
		var originalLoginId = $("#originalLoginId").val();
		var regex = /^[a-zA-Z0-9]{6,20}$/
		loginIdInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(value)){
			error ="ログインIDを入力してください";
		}else if(!value.match(regex)){
			error = "ログインIDは6文字以上20文字以下の半角英数字で入力してください";
		} else {
			$.ajax({
				type: "GET",
				url: "./ajax_setting",
				data: {requestJs : value, originalLoginId : originalLoginId},
			}).done(function(data){
				error = data.responseMessage;
				loginIdInput.siblings(".error").text(error).css('color', 'red');
				checkErrors();
			}).fail(function(data){
				alert("リクエスト時になんらかのエラーが発生しました");
			});
		}
		loginIdInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});

	passwordInput.blur(function(){
		var error = "";
		var secondPasswordError = "";
		var password = passwordInput.val();
		var secondPassword = secondPasswordInput.val();
		var regex = /^([a-zA-Z0-9!-/:-@¥[-`{-~]{6,20})$/
		passwordInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(password) && !$.isBlank(secondPassword)){
			secondPasswordInput.siblings(".error").text("パスワードが一致していません").css('color', 'red');
		}
		if(!$.isBlank(password) && !password.match(regex)){
			error = "パスワードは6文字以上20文字以下の記号を含む半角文字で入力してください";
		}else if(password !== secondPassword){
			secondPasswordInput.siblings(".error").text( "パスワードが一致していません").css('color', 'red');
		}
		passwordInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});

	secondPasswordInput.blur(function(){
		var error = "";
		var password = passwordInput.val();
		var secondPassword = secondPasswordInput.val();
		secondPasswordInput.siblings(".error").text("").css('color', 'red');
		if((!$.isBlank(password) || !$.isBlank(secondPassword)) && password !== secondPassword){
			error = "パスワードが一致していません";
		}else{
			error = "";
		}
		secondPasswordInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});

	nameInput.blur(function(){
		var error = "";
		var name = nameInput.val();
		nameInput.siblings(".error").text("").css('color', 'red');
		if($.isBlank(name)){
			error = "ユーザー名を入力してください";
		}else if(10 < name.length){
			error = "ユーザー名は10文字以下で入力してください";
		}else{
			error = "";
		}
		nameInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});

	branchInput.blur(function(){
		var error = "";
		var positionId = $("#positionForm option:selected").val();
		var branchId = $("#branchForm option:selected").val();
		positionInput.siblings(".error").text("").css('color', 'red');
		if(branchId != 1 && (positionId == 1 || positionId ==2 || positionId == 3)){
			error = "支店と部署・役職名の組み合わせが不適切です";
		}else if(branchId == 1 && (positionId == 5 || positionId == 6)){
			error = "支店と部署・役職名の組み合わせが不適切です";
		}else{
			error = "";
		}
		positionInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});

	positionInput.blur(function(){
		var error = "";
		var positionId = $("#positionForm option:selected").val();
		var branchId = $("#branchForm option:selected").val();
		positionInput.siblings(".error").text("").css('color', 'red');
		if(branchId != 1 && (positionId == 1 || positionId ==2 || positionId == 3)){
			error = "支店と部署・役職名の組み合わせが不適切です";
		}else if(branchId == 1 && (positionId == 5 || positionId == 6)){
			error = "支店と部署・役職名の組み合わせが不適切です";
		}else{
			error = "";
		}
		positionInput.siblings(".error").text(error).css('color', 'red');
		checkErrors();
	});
});
