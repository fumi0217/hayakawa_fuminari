
$(function(){
	$(".hiddenText").css("display", "none");
	$(".title").click(function(){
		var status = $(this).siblings(".hiddenText").css("display");
		/*var status = $(".hiddenText", this).css("display");*/
		if(status=="none"){
			$(this).siblings(".hiddenText").slideDown("slow");
		}else{
			$(this).siblings(".hiddenText").slideUp("slow");
		}



	});

	$(".title").mouseover(function(){
		$(this).parent().css("background-color", "white");
		$(this).css("cursor", "pointer");
	}).mouseout(function(){
		$(this).parent().css("background-color", "#D8CEF6");
	});

	$("span.tooltip").css({
		opacity: "0.9",
		position: "absolute",
		display: "none"
	});

	$("textarea.comment").mouseover(function(){
		$("span.tooltip").fadeIn();
	}).mouseout(function(){
		$("span.tooltip").fadeOut();
	}).mouseover(function(e){
		$("span.tooltip").css({
			"top": e.pageY + 10 + "px",
			"left": e.pageX + 10 + "px"
		});
	}).click(function(){
		$("span.tooltip").fadeOut();
	});

	$(".title").click(function(){
		$(".hiddenText").removeClass("selected");
		var created_date = $(this).siblings("#created_date").val();
		var createdDate = Date.parse(created_date);
		var now = new Date();
		var diffInSeconds = (now.getTime() - createdDate)/1000;

		var numOfWeeks = 0;
		var numOfDays = 0;
		var numOfHours = 0;
		var numOfMinutes = 0;
		var numOfSeconds = 0;
		var timeMessage;
		if(diffInSeconds > 1209600){
			$(this).siblings(".commentDate").text($(this.val()));
			return;
		}
		while(diffInSeconds >= 86400){
			numOfDays++;
			diffInSeconds -= 86400;
		}
		while(diffInSeconds >= 3600){
			numOfHours++;
			diffInSeconds -= 3600;
		}
		while(diffInSeconds >= 60){
			numOfMinutes++;
			diffInSeconds -= 60;
		}
		numOfSeconds = Math.round(diffInSeconds);

		if(numOfWeeks != 0){
			timeMessage = numOfWeeks+"週間"
		}else if(numOfDays != 0){
			timeMessage = numOfDays+"日";
		}else if(numOfHours != 0){
			timeMessage = numOfHours+"時間";
		}else if( numOfMinutes != 0){
			timeMessage = numOfMinutes+"分";
		}else{
			timeMessage = numOfSeconds+"秒";
		}
		timeMessage = timeMessage + "前";
		$(this).siblings(".hiddenText").addClass("selected");
		$(".selected .calculatedTime").text(timeMessage);

		/*var sample =  $(this).siblings(".hiddenText").children(".commentsArea").find(".created_date");
		sample.each(function(){
			var target = $(this).val();
		});*/
		var created_dates = $(this).siblings(".hiddenText").children(".commentsArea").find(".created_date");
		created_dates.each(function(){
			var commentDate = Date.parse($(this).val());
			diffInSeconds = (now.getTime() - commentDate)/1000;

			numOfWeeks = 0;
			numOfDays = 0;
			numOfHours = 0;
			numOfMinutes = 0;
			numOfSeconds = 0;
			timeMessage;
			if(diffInSeconds > 1209600){
				$(this).siblings(".commentDate").text($(this.val()));
				return;
			}
			while(diffInSeconds >= 604800){
				numOfWeeks++;
				diffInSeconds -= 604800;
			}
			while(diffInSeconds >= 86400){
				numOfDays++;
				diffInSeconds -= 86400;
			}
			while(diffInSeconds >= 3600){
				numOfHours++;
				diffInSeconds -= 3600;
			}
			while(diffInSeconds >= 60){
				numOfMinutes++;
				diffInSeconds -= 60;
			}
			numOfSeconds = Math.round(diffInSeconds);

			if(numOfWeeks != 0){
				timeMessage = numOfWeeks+"週間"
			}else if(numOfDays != 0){
				timeMessage = numOfDays+"日";
			}else if(numOfHours != 0){
				timeMessage = numOfHours+"時間";
			}else if( numOfMinutes != 0){
				timeMessage = numOfMinutes+"分";
			}else{
				timeMessage = numOfSeconds+"秒";
			}
			timeMessage = timeMessage + "前";
			$(this).siblings(".commentDate").text(timeMessage);
		});

	});
});