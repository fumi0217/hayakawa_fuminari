<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>ホーム画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	function disp(message){
		if(window.confirm(message + 'の投稿を削除しても、本当によろしいですか？(´,,•ω•,,`)')){
			if(window.confirm('あなたは過去のあなたを否定するような人だったんですか？(ﾟДﾟ)')){
				alert('見損ないました。信じていたのに、、、。・°°・(＞_＜)・°°・。');
				return true;
			}else{
				alert('ちょろすぎでしょ( ˘ω˘ )ｽﾔｧ…');
				return false;
			}
		}else{
			alert('冷やかしなら、最初から押すなよ、(｡◟‸◞｡✿)');
			return false;
		}
	}

	function disp2(comment){

		if(window.confirm('コメント本文：' + comment + 'のコメントを削除しても、本当によろしいですか？(´,,•ω•,,`)')){
			if(window.confirm('あなたは過去のあなたを否定するような人だったんですか？(ﾟДﾟ)')){
				alert('見損ないました。信じていたのに、、、。・°°・(＞_＜)・°°・。');
				return true;
			}else{
				alert('ちょろすぎでしょ( ˘ω˘ )ｽﾔｧ…');
				return false;
			}
		}else{
			alert('冷やかしなら、最初から押すなよ、(｡◟‸◞｡✿)');
			return false;
		}
	}
</script>
</head>
<body id="home">
<div id="wrap">
<p id="welcome">ようこそ、${loginUser.name}さん</p>
	<div class="contents clearfix">

		<div class="sidebar">

			<ul class="global-nav">
				<h2>メニュー</h2>
				<li><a href="new_messages">新規投稿画面</a></li>
				<c:if test="${loginUser.getBranchId()==1 && loginUser.getPositionId()==1 || loginUser.getPositionId()==2 }">
					<li><a href="user_management?page=1&column=created_date&order=desc">ユーザー管理画面</a></li>
				</c:if>
				<li><a href="logout">ログアウトする</a>
			</ul>

			<div id="sorting">
				<form action="./">
					<div class="category">
					<label for="category">カテゴリ</label>
					<input type="text" name="category" value="${category}" style="height: 25px;" placeholder="カテゴリを入力してみよう"></div><br>
					<div class="term"><label for="startDate">開始期間</label>
					<input type="date" name="startDate" value="${startDate}"></div><br>
					<div class="term"><label for="endDate">終了期間</label>
					<input type="date" name="endDate" value="${endDate}"></div><br>
					<button type="submit" class="btn">検索</button><a href="./" class="btn" id="resetBtn">リセット</a>
				</form>

			</div>
		</div>


		<div class="main-contents">

			<div class="error">${errorMessages}</div>

			<c:choose>
				<c:when test="${messages.size() == 0}">
					投稿が見つかりませんでした
				</c:when>
				<c:otherwise>
					${numOfMessages+(pages-1)*20}件の投稿が見つかりました
				</c:otherwise>
			</c:choose>
			<span id="numMessages" style="text-align=right">${20*(page-1)+1}～
				<c:choose>
					<c:when test="${page*20>numOfMessages+(pages-1)*20}">
						${numOfMessages+(pages-1)*20}件
					</c:when>
					<c:otherwise>
						${page*20}件
					</c:otherwise>
				</c:choose>
			</span>

			<c:forEach items="${messages}" var="message">


				<div class="messageArea">

					<li class="icon title"><c:out value="${message.getTitle()}"></c:out></li>
					<input type="hidden" value="${message.createdDate}" id="created_date">
					<div class="hiddenText">
						<input type="hidden" name="messageId" id="messageId" value="${message.getId()}">
						<li style="display=inline">投稿者:${message.getUserName()} <span class="calculatedTime"> ${message.getCreatedDate()}</span><%-- <fmt:formatDate value="${message.getCreatedDate()}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate> --%></li>


						<li>カテゴリー:<c:out value="${message.getCategory()}"></c:out></li>
						<li>本文:<br>
							<c:forEach var="text" items="${fn:split(message.getText(),'
							')}">
								<c:out value="${text}"></c:out><br>
							</c:forEach>
						</li>


						<c:if test="${loginUser.getId() == message.getUserId() || loginUser.positionId == 3 || loginUser.positionId == 5 && message.branchId == loginUser.branchId}">
							<form action="message_delete" method="post">
								<input type="hidden" name="messageId" value="${message.getId()}">
								<button type="submit" class="longBtn" onClick="return disp('${message.title}');">投稿を削除する</button>
							</form>
						</c:if>
						<div class="commentsArea">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${comment.getMessageId() == message.getId()}">
									<li id="comment-area-${message.id}" class="commentArea">
										<input type="hidden" class="created_date" value="${comment.createdDate}">
										<c:forEach var="text" items="${fn:split(comment.getText(),'
										')}">
											<c:out value="${text}"></c:out><br>
										</c:forEach>
										コメント者：${comment.getUserName()} <span class="commentDate"></span><%-- <fmt:formatDate value="${comment.getCreatedDate()}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate> --%><br>
										<c:if test="${loginUser.getId() == comment.getUserId() || loginUser.positionId == 3 || loginUser.positionId == 5 && comment.branchId == loginUser.branchId}">
											<form action="comment_delete" method="post">
												<input type="hidden" name="commentId" value="${comment.getId()}">
												<button type="submit" class="longBtn" id="deleteBtn" onClick="return disp2('${comment.text}');">コメントを削除する</button>
											</form>
										</c:if>
									</li>
									<br>
								</c:if>
							</c:forEach>
						</div>




						<form action="./" method="post" id="comment-form">
							<c:choose>
								<c:when test="${message.id == commentedMessageId}">
									<textarea name="commentText" rows="5" cols="80" class="comment">${text}</textarea>
								</c:when>
								<c:otherwise>
									<textarea name="commentText" rows="5" cols="80" class="comment" placeholder="コメントの本文を入力してみてね☆"></textarea>
								</c:otherwise>
							</c:choose>
							<span class="tooltip">ここにコメントの本文を500文字以下で入力すると、<br>下記の「コメントする」ボタンが押せるようになります</span>
							<p style="height: 30px;" class="error"><c:if test="${message.id == commentedMessageId}">${errorMessage}</c:if></p>
							<input type="hidden" name="messageId" value="${message.getId()}">
							<input type="submit" value="コメントする" class="longBtn commentBtn" id="commentBtn">
						</form>
					</div>

			</div>
			</c:forEach>
			<div class="paging">
				<a href="./?page=1&category=${category}&startDate=${startDate}&endDate=${endDate}">1</a>
				<c:forEach begin="2" end="${pages}" varStatus="status">
					/ <a href="./?page=${status.index}&category=${category}&startDate=${startDate}&endDate=${endDate}">${status.index}</a>
				</c:forEach>
			</div>
			<c:remove var="messages" scope="session"></c:remove>
			<c:remove var="comments" scope="session"></c:remove>
			<c:remove var="text" scope="session"></c:remove>
			<c:remove var="errorMessage" scope="session"></c:remove>
			<c:remove var="errorMessages" scope="session"></c:remove>
			<c:remove var="commentedMessageId" scope="session"></c:remove>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>


<script src="./js/autoCommentError.js"></script>
<script src="./js/showMessages.js"></script>
</body>
</html>