<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>ユーザー管理画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	function dispForStop(form){
		var name = form.name.value;
		if(window.confirm(name + 'さんを停止させますか？')){
			if(window.confirm('あなたは' + name + 'さんに半永久的に恨まれることになるかもしれませんがよろしいでしょうか？')){
				alert('駅では後ろに気をつけてください');
				return true;
			}else{
				alert('賢明な判断です');
				return false;
			}
		}else{
			alert('触らぬ神に祟りなしです');
			return false;
		}
	}

</script>
<script type="text/javascript">
	function dispForRecover(form){
		var name = form.name.value;
		if(window.confirm(name + 'さんを復活させますか？')){
			if(window.confirm('復活のアイテムは世界樹の尾ですか？')){
				alert('にわかですね');
				return false;
			}else{
				alert('正解です');
				return true;
			}
		}else{
			alert('貧乏人かー');
			return false;
		}
	}
</script>
</head>
<body id="user-management">
<div id="wrap">
	<div class="contents clearfix">
		<div class="sidebar">
			<ul class="global-nav">
				<h2>メニュー</h2>
				<li><a href="./?page=1&category=&startDate=&endDate=">ホーム</a>
				<li><a href="signup">新規登録画面</a></li>
				<li><a href="logout">ログアウトする</a></li>
			</ul>

			<div id="sorting">
				<form action="user_management">
					<input type="hidden" name="page" value="1">
					<div class="column">
						<label for="column">項目名</label>
						<select name="column" id="column">
							<c:choose>
								<c:when test="${column=='login_id'}">
									<option value="login_id" selected>ログインID</option>
									<option value="created_date">登録日時</option>
								</c:when>
								<c:otherwise>
									<option value="login_id">ログインID</option>
									<option value="created_date" selected>登録日時</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div><br>
					<div class="order">
						<label for="order">並び順</label>
						<select name="order" id="order">
							<c:choose>
								<c:when test="${order=='asc'}">
									<option value="asc" selected>昇順</option>
									<option value="desc">降順</option>
								</c:when>
								<c:otherwise>
									<option value="asc">昇順</option>
									<option value="desc" selected>降順</option>
								</c:otherwise>
							</c:choose>
						</select>
					</div><br>

					<button type="submit" class="btn">並び替える</button>
					<!-- <label for="branch">支店名</label>
					<input type="text" id="branch" name="branch"><br>
					<label for="position">部署・役職名</label>
					<input type="text" id="branch" name="position"><br>
					<button type="submit" class="btn">検索する</button> -->
				</form>


			</div>
		</div>

		<div class="main-contents">
			<div class="errorMessages">
				<c:if test="${not empty errorMessages}">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li>${message}</li>
						</c:forEach>
					</ul>
				<c:remove var="errorMessages" scope="session"></c:remove>
				</c:if>
			</div>
			<table>
				<tr style="border: 2px solid">
					<th style="width: 192px;">ログインID</th>
					<th style="width: 89px; border: 2px solid">氏名</th>
					<th style="width: 104px;">支店名</th>
					<th style="width: 99px; height: 70px; border: 2px solid">部署・役職名</th>
					<th>編集</th>
					<th style="width: 114px; border: 2px solid"">アカウント状態</th>
				</tr>
				<c:forEach items="${users}" var="user">
					<c:choose>
						<c:when test="${user.getIsStopped() == 0}">
							<tr>
								<td>${user.getLoginId()}</td>
								<td>${user.getName()}</td>
								<td>${user.getBranchName()}</td>
								<td>${user.getPositionName()}</td>
								<td>
									<input type="hidden" name="name" value="${user.name}">
									<a href="user_setting?id=${user.getId()}" class="aBtn settingBtn">編集する</a>
								</td>
								<td>
									<form action="user_management" method="post">
										<c:choose>
											<c:when test="${loginUser.id==user.id}">
												<label>活動中</label>
											</c:when>
											<c:otherwise>
												<input type="hidden" name="id" value="${user.getId()}">
												<input type="hidden" name="name" value="${user.name}">
												<input type="hidden" name="isStopped" value="1">
												<span class="is-stopped"><label>活動中</label><br>
												<input type=submit value="停止させる" class="longBtn stopBtn" onClick="return dispForStop(this.form);"></span>
											</c:otherwise>
										</c:choose>
									</form>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr class="stopped-user">
								<td>${user.getLoginId()}</td>
								<td>${user.getName()}</td>
								<td>${user.getBranchName()}</td>
								<td>${user.getPositionName()}</td>
								<td>
									<input type="hidden" name="name" value="${user.name}">
									<a href="user_setting?id=${user.getId()}" class="aBtn settingBtn">編集する</a>
								</td>
								<td>
									<form action="user_management" method="post">
										<c:choose>
											<c:when test="${loginUser.id==user.id}">
												<label>停止中</label>
											</c:when>
											<c:otherwise>
												<input type="hidden" name="id" value="${user.getId()}">
												<input type="hidden" name="name" value="${user.name}">
												<input type="hidden" name="isStopped" value="0">
												<span class="is-stopped"><label>停止中</label><br>
												<input type="submit" value="復活させる" class="longBtn" onClick="return dispForRecover(this.form)"></span>
											</c:otherwise>
										</c:choose>
									</form>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</table>
			<div class="paging">
				<a href="user_management?page=1&column=${column}&order=${order}">1</a>
				<c:forEach begin="2" end="${pages}" varStatus="status">
					/ <a href="user_management?page=${status.index}&column=${column}&order=${order}">${status.index}</a>
				</c:forEach>
			</div>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>
</div>
</body>
</html>