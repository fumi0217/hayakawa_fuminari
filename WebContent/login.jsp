<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>ログイン画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body id="login">
<div id="wrap">
	<div class="contents">
		<div class="main-center">


			<div id="login-form">
				<strong>BBS</strong>
				<div class="errorMessages">${errorMessages}</div>
				<c:remove var="errorMessages" scope="session"></c:remove>
				<form action="login" method="post">
					<div class="login-id"><label for="loginId">ログインID</label><input id="login-id" name="loginId" value="${loginId}"><br>
					<p class="error" style="height: 1px; color: red;"></p></div>
					<div class="password"><label for="password">パスワード</label><input id="password" type="password" name="password"><br>
					<p class="error" style="height: 1px; color: red;"></p></div>
					<input class="btn" type="submit" value="ログイン" id="loginBtn">
				</form>
			</div>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>
</div>
<script src="js/autoLoginError.js"></script>
</body>
</html>