<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>${originalName}の編集画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body id="user-setting">
<div id="wrap">
	<div class="contents clearfix">
		<div class="sidebar">

			<ul class="global-nav">
				<h2>メニュー</h2>
				<li><a href="./?page=1&category=&startDate=&endDate=">ホーム</a></li>
				<li><a href="user_management?page=1&column=created_date&order=desc">ユーザー管理画面</a></li>
				<li><a href="logout">ログアウトする</a></li>
			</ul>
		</div>
		<div class="main-contents">

			<form action="user_setting" method="post" id="setting-form">
				<div id="nameForm">
					<label for="name">氏名：</label><br>
					<input name="name" id="name" class="name" value="${edittedUser.getName()}">
					<p class="error" style="height: 15px;">${userNameErrorMessage}</p>
				</div>
				<div id="loginIdForm">
					<label for="loginId">ログインID：</label><br>
					<input name="loginId" id="login_id" class="login_id" value="${edittedUser.getLoginId()}">
					<p class="error" style="height: 15px;">${loginIdErrorMessage}</p>
				</div>
				<div id="passwordForm">
					<label for="password">パスワード：</label><br>
					<input type="password" name="password" id="password" class="password">
					<p class="error" style="height: 15px;">${passwordErrorMessage}</p>
				</div>
				<div id="secondPasswordForm">
					<label for="secondPassword">パスワード(確認用)：</label><br>
					<input type="password" name="secondPassword" id="second_password" class="second_password">
					<p class="error" style="height: 15px;">${secondPasswordErrorMessage}</p>
				</div>
				<div id="branchForm">
					<label for="branch">支店名：</label>
					<c:choose>
						<c:when test="${isSameUser}">
							<input type="hidden" name="branch" value="${edittedUser.getBranchId()}">
							${edittedUser.getBranchName()}
						</c:when>
						<c:otherwise>
							<br><select name="branch" id="branch" class="branch">
								<c:forEach items="${branches}" var="branch">
									<c:if test="${edittedUser.getBranchId() == branch.getId() }">
										<option value="${branch.getId()}" selected>${branch.getName()}</option>
									</c:if>
									<c:if test="${edittedUser.getBranchId() != branch.getId() }">
										<option value="${branch.getId()}">${branch.getName()}</option>
									</c:if>
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				<p class="error" style="height: 15px;"></p>
				<div id="positionForm">
					<label for="position">部署・役職名：</label>
					<c:choose>
						<c:when test="${isSameUser}">
							<input type="hidden" name="position" value="${edittedUser.getPositionId()}">
							${edittedUser.getPositionName()}

						</c:when>
						<c:otherwise>
							<br><select name="position" id="position" class="position">
								<c:forEach items="${positions}" var="position">
									<c:if test="${edittedUser.getPositionId() == position.getId()}">
										<option value="${position.getId()}" selected>${position.getName()}</option>
									</c:if>
									<c:if test="${edittedUser.getPositionId() != position.getId()}">
										<option value="${position.getId()}">${position.getName()}</option>
									</c:if>
								</c:forEach>
							</select>
						</c:otherwise>
					</c:choose>
				</div>
				<p class="error" style="height: 15px;">${positionErrorMessage}</p>
				<input type="hidden" name="id" value="${edittedUser.getId()}">
				<input type="hidden" name="originalLoginId" id="originalLoginId" value="${edittedUser.getLoginId()}">
				<input type="hidden" name="originalPassword" value="${edittedUser.getEncPassword()}">
				<input type="hidden" name="originalName" value="${originalName}">
				<input type="submit" value="更新する" class="btn" id="settingBtn">
			</form>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>
</div>

<script src="js/autoUserSettingError.js"></script>
</body>
</html>