<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>新規投稿画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
	function disp(){
		if(window.confirm('この内容で投稿してよろしいですか？')){
			if(window.confirm('この駄文を投稿しても、本当に大丈夫なんですか？')){
				return window.confirm('これが最後の警告になりますが、本当によろしいですか？')
			}else{
				if(window.confirm('えっ、、ここで投稿をやめてしまうんですか？')){
					alert('その程度の気持ちなら、最初からしないでください');
					retrun false;
				}else{
					alert('あなたなら、本当の勇者になれると信じていました');
					return false;
				}
			}
			return false;
		}
	}
</script>
</head>
<body id="new-messages">
<div id="wrap">
	<div class="contents clearfix">

		<div class="sidebar">

			<ul class="global-nav">
				<h2>メニュー</h2>
				<li><a href="./?page=1&category=&startDate=&endDate=">ホーム</a>
				<li><a href="logout">ログアウトする</a>
			</ul>
		</div>
		<div class="main-contents">
			<div class="new-messages">
				<form action="new_messages" method="post" id="message-form">
					<div id="titleForm">
						<label for="title">タイトル</label><br>
						<input name="title" id="title" class="title" value="${title}">
						<p class="error" style="height: 1px;">
							${titleErrorMessage}
						</p>
					</div>
					<br>
					<div id="categoryForm">
						<label for="category">カテゴリー</label><br>
						<input name="category" id="category" class="category" value="${category}">
						<p class="error" style="height: 1px;">
							${categoryErrorMessage}
						</p>
					</div>
					<br>
					<div id="textForm">
						<label for="text">テキスト</label>
						<textarea name="text" id="text" class="text" cols="80" rows="10">${text}</textarea>
						<p class="error" style="height: 1px;">
							${textErrorMessage}
						</p>
					</div>
					<br>
					<input type="submit" value="新規投稿" class="btn" id="newMessageBtn" onClick="return disp()">
					<c:remove var="title" scope="session"></c:remove>
					<c:remove var="text" scope="session"></c:remove>
					<c:remove var="category" scope="session"></c:remove>
				</form>
			</div>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>
</div>
<script src="js/autoMessageError.js"></script>

</body>
</html>