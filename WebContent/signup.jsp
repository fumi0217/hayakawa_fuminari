<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>新規登録画面</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body id="signup">
	<div id="wrap">
		<div class="contents clearfix">
			<div class="sidebar">

				<ul class="global-nav">
					<h2>メニュー</h2>
					<li><a href="./?page=1&category=&startDate=&endDate=">ホーム</a></li>
					<li><a href="user_management?page=1&column=created_date&order=desc">ユーザー管理画面</a></li>
					<li><a href="logout">ログアウトする</a></li>
				</ul>
			</div>
		<div class="main-contents">

			<div class="signup-form">
				<h2>ユーザー新規登録</h2>
				<form action="signup" method="post" id="signup-form">
					<div id="nameForm">
						<label for="name">氏名:</label><br>
						<input name="name" id="name" class="name" value="${name}">
						<p class="error" style="height: 1px; color: red;">${userNameErrorMessage}</p>
					</div>
					<br>
					<div id="loginIdForm">
						<label for="login_id">ログインID:</label><br>
						<input name="login_id" id="login_id" class="login_id" value="${loginId}">
						<p class="error" style="height: 15px; color: red;">${loginIdErrorMessage}</p>
					</div>
					<br>
					<div id="passwordForm">
						<label for="password">パスワード:</label><br>
						<input type="password" name="password" id="password" class="password">
						<p class="error" style="height: 1px; color: red;">${passwordErrorMessage}</p>
					</div>
					<br>
					<div id="secondPasswordForm">
						<label for="second_password">パスワード(確認用):</label><br>
						<input type="password" name="second_password" id="second_password" class="secondPassword">
						<p class="error" style="height: 1px; color: red;">${secondPasswordErrorMessage}</p>
					</div>
					<br>
					<div id="branchForm">
						<label for="branch">支店名：</label><br>
						<select name="branch" id="branch" class="branch">
							<option value="-1">支店名を選択してください</option>
							<c:forEach items="${branches}" var="branch">
								<c:if test="${branchId == branch.getId() }">
									<option value="${branch.getId()}" selected>${branch.getName()}</option>
								</c:if>
								<c:if test="${branchId != branch.getId() }">
									<option value="${branch.getId()}">${branch.getName()}</option>
								</c:if>
							</c:forEach>
						</select>
						<p class="error" style="height: 1px; color: red;">${branchErrorMessage}</p>
					</div>
					<br>
					<div id="positionForm">
						<label for="position">部署・役職名：</label><br>
						<select name="position" id="position" class="position">
							<option value="-1">部署・役職名を選択してください</option>
							<c:forEach items="${positions}" var="position">
								<c:if test="${positionId == position.getId() }">
									<option value="${position.getId()}" selected>${position.getName()}</option>
								</c:if>
								<c:if test="${positionId != position.getId() }">
									<option value="${position.getId()}">${position.getName()}</option>
								</c:if>
							</c:forEach>
						</select>
						<p class="error" style="height: 1px; color: red;">${positionErrorMessage}</p>
					</div>
					<br>
					<input type="submit" value="新規登録" class="btn" id="signupBtn">
				</form>
			</div>
		</div>
	</div>
<div class="footer"><small>Copyright(c)Fuminari Hayakawa</small></div>
</div>

<script src="js/autoUserError.js"></script>
</body>
</html>