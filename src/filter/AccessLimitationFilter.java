package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


/**
 * Servlet Filter implementation class AccessLimitationFilter
 */

@WebFilter(filterName="accessLimitationFilter")
public class AccessLimitationFilter implements Filter {
	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
		HttpSession session = req.getSession();
		User loginUser = (User)session.getAttribute("loginUser");
		String path = req.getServletPath();
		/*if(!path.equals("/login") && !path.equals("./") && !path.equals("/index.jsp") && !path.equals("/new_messages") && !path.endsWith("css") && !path.endsWith("js")){*/
		if(path.equals("/user_management") || path.equals("/signup") || path.equals("/user_setting")){
			if(loginUser.getBranchId() != 1 || loginUser.getPositionId() != 1 && loginUser.getPositionId() != 2){
				session.setAttribute("errorMessages", "アクセス権限がありません");
				res.sendRedirect("./?page=1");
				return;
			}
		}
		chain.doFilter(request, response);
	}


	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
}


