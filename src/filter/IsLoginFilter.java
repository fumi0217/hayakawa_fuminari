package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet Filter implementation class IsLoginFilter
 */


@WebFilter(filterName="isLoginFilter")

public class IsLoginFilter implements Filter {

	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		String uri = req.getServletPath();
		if(!uri.equals("/login") && !uri.endsWith("css") && !uri.endsWith("js")){
			HttpSession session = req.getSession();
			if(session.getAttribute("loginUser")==null){
				HttpServletResponse res = (HttpServletResponse)response;
				session.setAttribute("errorMessages", "ログインしてください");
				res.sendRedirect("login");
				return;
			}
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}

