package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.MessageService;

/**
 * Servlet implementation class MessageDeleteServlet
 */
@WebServlet("/message_delete")
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int messageId = Integer.parseInt(request.getParameter("messageId"));
		MessageService messageService = new MessageService();
		messageService.deleteMessage(messageId);
		response.sendRedirect("./?page=1&category=&startDate=&endDate=");
	}

}
