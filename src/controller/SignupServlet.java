package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.GettingDataService;
import service.UserService;
import utils.CipherUtil;

/**
 * Servlet implementation class SignupServlet
 */
@WebServlet("/signup")
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branches = new ArrayList<Branch>();
		List<Position> positions = new ArrayList<Position>();
		GettingDataService gettingDataService = new GettingDataService();
		branches = gettingDataService.gettingBranches();
		positions = gettingDataService.gettingPositions();
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		String encPassword = CipherUtil.encrypt(password);


		if(isValid(request, errorMessages)){

			User signupUser = new User();
			signupUser.setLoginId(loginId);
			signupUser.setEncPassword(encPassword);
			signupUser.setName(name);
			signupUser.setBranchId(branch);
			signupUser.setPositionId(position);
			signupUser.setIsStopped(0);

			UserService userService = new UserService();
			userService.signup(signupUser);
			response.sendRedirect("user_management?page=1&column=created_date&order=desc");
		}else{
			List<Branch> branches = new ArrayList<Branch>();
			List<Position> positions = new ArrayList<Position>();
			GettingDataService gettingDataService = new GettingDataService();
			branches = gettingDataService.gettingBranches();
			positions = gettingDataService.gettingPositions();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("loginIdErrorMessage", errorMessages.get("loginIdErrorMessage"));
			request.setAttribute("passwordErrorMessage", errorMessages.get("passwordErrorMessage"));
			request.setAttribute("secondPasswordErrorMessage", errorMessages.get("secondPasswordErrorMessage"));
			request.setAttribute("userNameErrorMessage", errorMessages.get("userNameErrorMessage"));
			request.setAttribute("positionErrorMessage", errorMessages.get("positionErrorMessage"));
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("branchId", branch);
			request.setAttribute("positionId", position);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}


	private boolean isValid(HttpServletRequest request,  Map<String, String> errorMessages){
		List<String> existedLoginId = new ArrayList<String>();
		UserService userService = new UserService();
		existedLoginId = userService.getLoginId();
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");
		String secondPassword = request.getParameter("second_password");
		String name = request.getParameter("name");
		int branchId = Integer.valueOf(request.getParameter("branch"));
		int positionId = Integer.valueOf(request.getParameter("position"));


		//ログインidバリデーション
		String regex = "^[a-zA-Z0-9]{6,20}";
		if(StringUtils.isEmpty(loginId) || StringUtils.isBlank(loginId)){
			errorMessages.put("loginIdErrorMessage", "ログインIDを入力してください");
			/*loginIdErrorMessage = "ログインIDを入力してください";*/
		}else if(!loginId.matches(regex)){
			errorMessages.put("loginIdErrorMessage", "ログインIDは6文字以上20文字以下の半角英数字で入力してください");
			/*loginIdErrorMessage = "6文字以上20文字以下で入力してください";*/
		}else{
			for(String id : existedLoginId){
				if(loginId.equals(id)){
					errorMessages.put("loginIdErrorMessage", "ログインIDが重複しています");
					/*loginIdErrorMessage = "ログインIDが重複しています";*/
				}
			}
		}

		regex = "^[/_\\-+@*a-zA-Z0-9]{6,20}$";
		if(StringUtils.isEmpty(password) || StringUtils.isBlank(password)){
			errorMessages.put("passwordErrorMessage", "パスワードを入力してください");
			/*passwordErrorMessage = "パスワードを入力してください";*/
		}else if(!password.matches(regex)){
			errorMessages.put("passwordErrorMessage", "パスワードは6文字以上20文字以下の記号を含む半角文字で入力してください");
			/*passwordErrorMessage = "6文字以上20文字以下で入力してください";*/
		}

		if(StringUtils.isEmpty(secondPassword) || StringUtils.isBlank(secondPassword)){
			errorMessages.put("secondPasswordErrorMessage", "確認用パスワードを入力してください");
			/*secondPasswordErrorMessage = "確認用パスワードを入力してください";*/
		}else if(!password.equals(secondPassword)){
			errorMessages.put("secondPasswordErrorMessage", "パスワードが一致していません");
			/*secondPasswordErrorMessage = "パスワードが一致していません";*/
		}

		if(StringUtils.isEmpty(name) || StringUtils.isBlank(name)){
			errorMessages.put("userNameErrorMessage", "ユーザー名を入力してください");
			/*userNameErrorMessage = "ユーザー名を入力してください";*/
		}else if(name.length() > 10){
			errorMessages.put("userNameErrorMessage", "ユーザー名は10文字以下で入力してください");
			/*userNameErrorMessage = "ユーザー名は10文字以下で入力してください";*/
		}

		if(positionId == -1){
			errorMessages.put("positionErrorMessage", "部署・役職名を選択してください");
		}

		if(branchId == -1){
			errorMessages.put("branchErrorMessage", "支店名を選択してください");
		}

		if(((positionId == 1 || positionId ==2 || positionId == 3) && branchId != 1) || branchId == 1 && (positionId == 5 || positionId == 6)){
			errorMessages.put("positionErrorMessage", "支店と部署・役職名の組み合わせが不適切です");
			/*positionErrorMessage = "支店と部署・役職名の組み合わせが不適切です";*/
		}
		/*if(StringUtils.isEmpty(loginIdErrorMessage) && StringUtils.isEmpty(passwordErrorMessage) && StringUtils.isEmpty(secondPasswordErrorMessage) && StringUtils.isEmpty(userNameErrorMessage) && StringUtils.isEmpty(positionErrorMessage)){*/
		if(errorMessages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
