package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class AjaxServlet
 */
@WebServlet("/ajax_setting")
public class AjaxSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String REQUEST_STRING = "requestJs";
	private final String ORIGINAL = "originalLoginId";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter(REQUEST_STRING);
		String originalLoginId = request.getParameter(ORIGINAL);
    	User user = new User();
    	UserService userService = new UserService();
    	user = userService.getUser(loginId);
    	String responseJson;
    	if(user == null || originalLoginId.equals(loginId)){
    		responseJson = "{\"responseMessage\" : \"\"}";
    	}else{
    		responseJson = "{\"responseMessage\" : \"ログインIDが重複しています\"}";
    	}
    	response.setContentType("application/json;charset=UTF-8");
    	PrintWriter out = response.getWriter();
    	out.print(responseJson);

	}
}
