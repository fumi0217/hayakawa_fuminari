package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.GettingDataService;
import service.UserService;
import utils.CipherUtil;

/**
 * Servlet implementation class UserSettingServlet
 */
@WebServlet("/user_setting")
public class UserSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Branch> branches = new ArrayList<Branch>();
		List<Position> positions = new ArrayList<Position>();
		Boolean isSameUser = false;
		User loginUser = (User)request.getSession().getAttribute("loginUser");
		int loginUserId = loginUser.getId();
		GettingDataService gettingDataService = new GettingDataService();
		branches = gettingDataService.gettingBranches();
		positions = gettingDataService.gettingPositions();
		HttpSession session = request.getSession();


    	if(request.getParameter("id") == null){
    		session.setAttribute("errorMessages", "不正なパラメーターです");
    		response.sendRedirect("user_management?page=1&column=created_date&order=desc");
    		return;
    	}

    	int edittedUserId = Integer.valueOf(request.getParameter("id"));
    	User edittedUser = new User();
    	UserService userService = new UserService();
    	edittedUser = userService.getUserInformation(edittedUserId);

    	if(edittedUser == null){
    		session.setAttribute("errorMessages", "不正なパラメーターです");
    		response.sendRedirect("user_management?page=1&column=created_date&order=desc");
    		return;
    	}

    	if(loginUserId == edittedUserId){
    		isSameUser = true;
    	}

    	request.setAttribute("originalName", edittedUser.getName());
    	request.setAttribute("isSameUser", isSameUser);
    	request.setAttribute("edittedUser", edittedUser);
    	request.setAttribute("branches", branches);
    	request.setAttribute("positions", positions);
		request.getRequestDispatcher("user_setting.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		int userId = Integer.valueOf(request.getParameter("id"));
		String newLoginId = request.getParameter("loginId");
		String newName = request.getParameter("name");
		int branchId = Integer.valueOf(request.getParameter("branch"));
		int positionId = Integer.valueOf(request.getParameter("position"));

		if(isValid(request, errorMessages)){

			String newPassword = passwordIsNull(request);

			User updatedUser = new User();
			updatedUser.setId(userId);
			updatedUser.setLoginId(newLoginId);
			updatedUser.setName(newName);
			updatedUser.setEncPassword(newPassword);
			updatedUser.setBranchId(branchId);
			updatedUser.setPositionId(positionId);
			UserService userService = new UserService();
			userService.updateUser(updatedUser);
			response.sendRedirect("user_management?page=1&column=created_date&order=desc");
		}else{
			List<Branch> branches = new ArrayList<Branch>();
			List<Position> positions = new ArrayList<Position>();
			Boolean isSameUser = false;
			User loginUser = (User)request.getSession().getAttribute("loginUser");
			int loginUserId = loginUser.getId();
			GettingDataService gettingDataService = new GettingDataService();
			branches = gettingDataService.gettingBranches();
			positions = gettingDataService.gettingPositions();
			HttpSession session = request.getSession();
			int edittedUserId = Integer.valueOf(request.getParameter("id"));
	    	User edittedUser = new User();
	    	UserService userService = new UserService();
	    	edittedUser = userService.getUserInformation(edittedUserId);

	    	if(edittedUser == null){
	    		session.setAttribute("errorMessages", "不正なパラメーターです");
	    		response.sendRedirect("user_management?page=1&column=created_date&order=desc");
	    		return;
	    	}

	    	if(loginUserId == edittedUserId){
	    		isSameUser = true;
	    	}

	    	request.setAttribute("isSameUser", isSameUser);
	    	request.setAttribute("edittedUser", edittedUser);
	    	request.setAttribute("branches", branches);
	    	request.setAttribute("positions", positions);
			request.setAttribute("loginIdErrorMessage", errorMessages.get("loginIdErrorMessage"));
			request.setAttribute("passwordErrorMessage", errorMessages.get("passwordErrorMessage"));
			request.setAttribute("secondPasswordErrorMessage", errorMessages.get("secondPasswordErrorMessage"));
			request.setAttribute("userNameErrorMessage", errorMessages.get("userNameErrorMessage"));
			request.setAttribute("positionErrorMessage", errorMessages.get("positionErrorMessage"));
			request.setAttribute("id", userId);
			request.setAttribute("originalName", request.getParameter("originalName"));
			request.getRequestDispatcher("user_setting.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, Map<String, String> errorMessages){
		String password = request.getParameter("password");
		String secondPassword = request.getParameter("secondPassword");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		int branchId = Integer.valueOf(request.getParameter("branch"));
		int positionId = Integer.valueOf(request.getParameter("position"));
		List<String> existedLoginId = new ArrayList<String>();
		UserService userService = new UserService();
		existedLoginId = userService.getLoginId();
		String originalLoginId = request.getParameter("originalLoginId");

		/*loginId.matches("^[/_\\-+@*a-zA-Z0-9]{6,20}$")*/
		String regex = "^[a-zA-Z0-9]{6,20}";
		if(StringUtils.isEmpty(loginId) || StringUtils.isBlank(loginId)){
			errorMessages.put("loginIdErrorMessage", "ログインIDを入力してください");
		}else if(!loginId.matches(regex)){
			errorMessages.put("loginIdErrorMessage", "ログインIDは6文字以上20文字以下の半角英数字で入力してください");
		}else if(!loginId.equals(originalLoginId)){
			for(String id : existedLoginId){
				if(loginId.equals(id)){
					errorMessages.put("loginIdErrorMessage", "ログインIDが重複しています");
				}
			}
		}

		if(StringUtils.isEmpty(name) || StringUtils.isBlank(name)){
			errorMessages.put("userNameErrorMessage", "ユーザー名を入力してください");
		}else if(name.length() > 10){
			errorMessages.put("userNameErrorMessage", "ユーザー名は10文字以下で入力してください");
		}

		/*!password.matches("^[/_\\-+@*a-zA-Z0-9]{6,20}$")*/
		regex = "^[/_\\-+@*a-zA-Z0-9]{6,20}$";
		if(!password.matches(regex)){
			errorMessages.put("passwordErrorMessage", "パスワードは6文字以上20文字以下の記号を含む半角文字で入力してください");
		}


		if(!StringUtils.isEmpty(password) && StringUtils.isEmpty(secondPassword)){
			errorMessages.put("secondPasswordErrorMessage", "確認用パスワードを入力してください");
		}else if(!password.equals(secondPassword)){
			errorMessages.put("secondPasswordErrorMessage", "パスワードが一致していません");
		}

		if(((positionId == 1 || positionId ==2 || positionId == 3) && branchId != 1) || branchId == 1 && (positionId == 5 || positionId == 6)){
			errorMessages.put("positionErrorMessage", "支店と部署・役職名の組み合わせが不適切です");
		}

		if(errorMessages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

	private String passwordIsNull(HttpServletRequest request){
		String password = request.getParameter("password");
		if(StringUtils.isEmpty(password)){
			password = request.getParameter("originalPassword");
			return password;
		}else{

		}
		return CipherUtil.encrypt(password);
	}
}
