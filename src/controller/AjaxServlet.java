package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

/**
 * Servlet implementation class AjaxServlet
 */
@WebServlet("/ajax")
public class AjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String REQUEST_STRING = "requestJs";
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loginId = request.getParameter(REQUEST_STRING);
    	User user = new User();
    	UserService userService = new UserService();
    	user = userService.getUser(loginId);
    	String responseJson;
    	if(user == null){
    		responseJson = "{\"responseMessage\" : \"\"}";
    	}else{
    		responseJson = "{\"responseMessage\" : \"ログインIDが重複しています\"}";
    	}
    	response.setContentType("application/json;charset=UTF-8");
    	PrintWriter out = response.getWriter();
    	out.print(responseJson);

	}
}
