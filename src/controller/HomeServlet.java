package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Message;
import beans.User;
import service.CommentService;
import service.MessageService;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/index.jsp")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Message> messages = new ArrayList<Message>();
		List<Comment> comments = new ArrayList<Comment>();
		String category = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		MessageService messageService = new MessageService();
		int numOfMessages = messageService.countMessages(category, startDate, endDate);
		int pages = 1;
		while(numOfMessages>20){
			numOfMessages -= 20;
			pages++;
		}
		request.setAttribute("pages", pages);
		int showingPageNum = Integer.parseInt(request.getParameter("page"));
		int startLine = 20 * (showingPageNum - 1);
		messages = messageService.sortMessages(category, startDate, endDate, startLine);
		CommentService commentService = new CommentService();
		comments = commentService.getComments();
		request.setAttribute("page", showingPageNum);
		request.setAttribute("category", category);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("numOfMessages", numOfMessages);
		request.getRequestDispatcher("home.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> errorMessage = new HashMap<String, String>();
		HttpSession session = request.getSession();
		int messageId = Integer.parseInt(request.getParameter("messageId"));
		String text = request.getParameter("commentText");
		User loginUser = (User)session.getAttribute("loginUser");
		if(isValid(request, errorMessage)){
			Comment newComment = new Comment();
			newComment.setMessageId(messageId);
			newComment.setText(text);
			newComment.setUserId(loginUser.getId());

			CommentService commentService = new CommentService();
			commentService.createNewComment(newComment);
			response.sendRedirect("./?page=1");
		}else{
			List<Message> messages = new ArrayList<Message>();
			List<Comment> comments = new ArrayList<Comment>();
			MessageService messageService = new MessageService();
			CommentService commentService = new CommentService();
			messages = messageService.getAllMessages();
			comments = commentService.getComments();
			session.setAttribute("messages", messages);
			session.setAttribute("comments", comments);
			session.setAttribute("errorMessage", errorMessage.get("commentErrorMessage"));
			session.setAttribute("text", text);
			session.setAttribute("commentedMessageId", messageId);
			response.sendRedirect("./?page=1#comment-area-" + messageId);
		}
	}

	private boolean isValid(HttpServletRequest request, Map<String, String> errorMessage){
		String text = request.getParameter("commentText");
		if(StringUtils.isBlank(text) || StringUtils.isEmpty(text)){
			errorMessage.put("commentErrorMessage", "コメントの本文を入力してください");
		}else if(text.length() > 500){
			errorMessage.put("commentErrorMessage", "本文は500文字以下で入力してください");
		}
		if(errorMessage.size() == 0){
			return true;
		}else{
			return false;
		}
	}

}
