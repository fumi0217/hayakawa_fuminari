package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
//import service.DownloadingDataService;
import service.UserService;

/**
 * Servlet implementation class UserManagementServlet
 */
@WebServlet("/user_management")
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService userService = new UserService();
		int totalNumUsers = userService.countUsers();
		int pages = 1;
		while(totalNumUsers>7){
			totalNumUsers -= 7;
			pages++;
		}
		request.setAttribute("pages", pages);
		int showingPageNum = Integer.parseInt(request.getParameter("page"));
		int startLine = 7 * (showingPageNum - 1);
		String column;
		String order;
		column = request.getParameter("column");
    	order = request.getParameter("order");
		request.setAttribute("column", column);
		request.setAttribute("order", order);

		List<User> users = new ArrayList<User>();
		users = userService.getAllUsers(String.valueOf(startLine), column, order);

		request.setAttribute("users", users);
		request.getRequestDispatcher("user_management.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int isStopped = Integer.valueOf(request.getParameter("isStopped"));
		int id = Integer.valueOf(request.getParameter("id"));
		User isStoppedUser = new User();
		isStoppedUser.setIsStopped(isStopped);
		isStoppedUser.setId(id);
		UserService userService = new UserService();
		userService.updateIsStopped(isStoppedUser);
		response.sendRedirect("user_management?page=1&column=created_date&order=desc");
	}
}
