package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

/**
 * Servlet implementation class NewMessagesServlet
 */
@WebServlet("/new_messages")
public class NewMessagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/new_messages.jsp").forward(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Map<String, String> errorMessages = new HashMap<String, String>();
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUser");
		int userId = loginUser.getId();

		if(isValid(request, errorMessages)){

			Message newMessage = new Message();
			newMessage.setTitle(title);
			newMessage.setText(text);
			newMessage.setCategory(category);
			newMessage.setUserId(userId);

			MessageService messageService = new MessageService();
			messageService.createNewMessage(newMessage);
			response.sendRedirect("./?page=1");

		}else{
			request.setAttribute("titleErrorMessage", errorMessages.get("titleErrorMessage"));
			request.setAttribute("textErrorMessage", errorMessages.get("textErrorMessage"));
			request.setAttribute("categoryErrorMessage", errorMessages.get("categoryErrorMessage"));
			request.setAttribute("title", title);
			request.setAttribute("text", text);
			request.setAttribute("category", category);
			request.getRequestDispatcher("new_messages.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, Map<String, String> errorMessages){
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");


		if(StringUtils.isEmpty(title) || StringUtils.isBlank(title)){
			errorMessages.put("titleErrorMessage", "タイトルを入力してください");
		}else if(title.length() > 30){
			errorMessages.put("titleErrorMessage", "タイトルは30文字以下で入力してください");
		}

		if(StringUtils.isEmpty(text) || StringUtils.isBlank(text)){
			errorMessages.put("textErrorMessage", "本文を入力してください");
		}else if(text.length() > 1000){
			errorMessages.put("textErrorMessage", "本文は1000文字以下で入力してください");
		}


		if(StringUtils.isEmpty(category) || StringUtils.isBlank(category)){
			errorMessages.put("categoryErrorMessage", "カテゴリーを入力してください");
		}else if(category.length() > 10){
			errorMessages.put("categoryErrorMessage", "カテゴリーは10文字以下で入力してください");
		}



		if(errorMessages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
