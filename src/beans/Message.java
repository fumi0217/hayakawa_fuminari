package beans;

import java.io.Serializable;

public class Message implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private String text;
	private String category;
	private String createdDate;
	private int userId;
	private String userName;
	private int positionId;
	private int branchId;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return this.id;
	}

	public int getBranchId(){
		return this.branchId;
	}

	public void setBranchId(int branchId){
		this.branchId = branchId;
	}

	public int getPositionId(){
		return this.positionId;
	}

	public void setPositionId(int positionId){
		this.positionId = positionId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return this.title;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return this.text;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return this.category;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return this.createdDate;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return this.userId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return this.userName;
	}

}
