package beans;

import java.io.Serializable;

public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String text;
	private String createdDate;
	private int userId;
	private String userName;
	private int positionId;
	private int messageId;
	private int branchId;


	public int getId(){
		return this.id;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getBranchId(){
		return this.branchId;
	}

	public void setBranchId(int branchId){
		this.branchId = branchId;
	}

	public int getPositionId(){
		return this.positionId;
	}

	public void setPositionId(int positionId){
		this.positionId = positionId;
	}

	public String getText(){
		return this.text;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getCreatedDate(){
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public int getUserId(){
		return this.userId;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public String getUserName(){
		return this.userName;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public int getMessageId(){
		return this.messageId;
	}

	public void setMessageId(int messageId){
		this.messageId = messageId;
	}
}
