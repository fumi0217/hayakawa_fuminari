package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class User implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String loginId;
	private String encPassword;
	private String name;
	private int branchId;
	private String branchName;
	private int positionId;
	private String positionName;
	private int isStopped;
	private Timestamp createdDate;
	private Timestamp updatedDate;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return this.id;
	}

	public void setLoginId(String loginId){
		this.loginId = loginId;
	}

	public String getLoginId(){
		return this.loginId;
	}

	public void setEncPassword(String encPassword){
		this.encPassword = encPassword;
	}

	public String getEncPassword(){
		return this.encPassword;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

	public void setBranchId(int branchId){
		this.branchId = branchId;
	}

	public int getBranchId(){
		return this.branchId;
	}

	public void setBranchName(String branchName){
		this.branchName = branchName;
	}

	public String getBranchName(){
		return this.branchName;
	}

	public void setPositionId(int positionId){
		this.positionId = positionId;
	}

	public int getPositionId(){
		return this.positionId;
	}

	public void setPositionName(String positionName){
		this.positionName = positionName;
	}

	public String getPositionName(){
		return this.positionName;
	}

	public void setIsStopped(int isStopped){
		this.isStopped = isStopped;
	}

	public int getIsStopped(){
		return isStopped;
	}

	public void setCreatedDate(Timestamp createdDate){
		this.createdDate = createdDate;
	}

	public Timestamp getCreatedDate(){
		return this.createdDate;
	}

	public void setUpdatedDate(Timestamp updatedDate){
		this.updatedDate = updatedDate;
	}

	public Timestamp getUpdatedDate(){
		return this.updatedDate;
	}

}
