package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import utils.CloseableUtil;
import beans.Branch;
import beans.Position;
import exception.SQLRuntimeException;

public class DownloadingData {
	public List<Branch> gettingBranches(Connection connection){
		String sql = "select * from branches;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Branch> branches = new ArrayList<Branch>();
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				Branch branch = new Branch();
				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branches.add(branch);
			}
			return branches;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
			CloseableUtil.close(rs);
		}
	}

	public List<Position> gettingPositions(Connection connection){
		String sql = "select * from positions;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Position> positions = new ArrayList<Position>();
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				Position position = new Position();
				position.setId(rs.getInt("id"));
				position.setName(rs.getString("name"));
				positions.add(position);
			}
			return positions;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
			CloseableUtil.close(rs);
		}
	}


}
