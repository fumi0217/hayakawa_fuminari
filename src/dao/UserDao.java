package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import utils.CloseableUtil;
import utils.DBUtil;

public class UserDao {
	public int countUsers(Connection connection){
		String sql = "select count(*) as count from users;";
		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getInt("count");
			}
			return count;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(rs);
			CloseableUtil.close(ps);
		}
	}
	public User getUser(Connection connection, String loginId){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if(userList.size() >= 2){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}

	public List<String> getLoginId(Connection connection){
		String sql = "select login_id from users;";
		PreparedStatement ps = null;
		List<String> loginId = new ArrayList<String>();
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				loginId.add(rs.getString("login_id"));
			}
			return loginId;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(rs);
			CloseableUtil.close(ps);
		}
	}
	public void updateIsStopped(Connection connection, User isStoppedUser){
		String sql = "update users set is_stopped = ?, updated_date = current_time where id = ?;";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, isStoppedUser.getIsStopped());
			ps.setInt(2, isStoppedUser.getId());
			int count = ps.executeUpdate();
	        if(count == 0){
	        	throw new NoRowsUpdatedRuntimeException();
	        }
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}


	public void update(Connection connection, User updatedUser){
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		try{
			sql.append("update users set name = ?, ");
			sql.append("login_id = ?, ");
			sql.append("password = ?, ");
			sql.append("branch_id = ?, ");
			sql.append("position_id = ?, ");
			sql.append("updated_date = current_time ");
			sql.append("where id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, updatedUser.getName());
			ps.setString(2, updatedUser.getLoginId());
			ps.setString(3, updatedUser.getEncPassword());
			ps.setInt(4, updatedUser.getBranchId());
			ps.setInt(5, updatedUser.getPositionId());
			ps.setInt(6, updatedUser.getId());

			int count = ps.executeUpdate();
            if(count == 0){
            	throw new NoRowsUpdatedRuntimeException();
            }

		}catch(SQLException e){
			DBUtil.rollback(connection);
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}


	public User getUserInformation(Connection connection, int userId){
		List<User> userList = new ArrayList<User>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "select * from users inner join branches on users.branch_id = branches.id "
				+ "inner join positions on users.position_id = positions.id where users.id = ?;";
		try{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);
			rs = ps.executeQuery();
			userList = toJoinUserList(rs);
			if(userList.isEmpty()){
				return null;
			}else if(userList.size() >= 2){
				throw new IllegalStateException("userList.size() >= 2");
			}
			return userList.get(0);
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
			CloseableUtil.close(rs);
		}
	}

	public List<User> getAllUsers(Connection connection, String startLine, String column, String order){
		if(StringUtils.isEmpty(column)){
			column = "created_date";
		}
		if(StringUtils.isEmpty(order)){
			order = "desc";
		}
		String sql = "select * from users inner join branches on users.branch_id = branches.id "
				+ "inner join positions on positions.id = users.position_id order by " + column + " " + order + " limit 7 offset " + startLine +";";

		PreparedStatement ps = null;
		ResultSet rs = null;
		List<User> userList = new ArrayList<User>();
		try{
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery(sql);

			userList = toJoinUserList(rs);

			if(userList.isEmpty()){
				return null;
			}

			return userList;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(rs);
			CloseableUtil.close(ps);
		}
	}


	public void insert(User signupUser, Connection connection){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("insert into users (");
			sql.append("login_id,");
			sql.append("password,");
			sql.append("name,");
			sql.append("branch_id,");
			sql.append("position_id,");
			sql.append("is_stopped");
			sql.append(")values(");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?);");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, signupUser.getLoginId());
			ps.setString(2, signupUser.getEncPassword());
			ps.setString(3, signupUser.getName());
			ps.setInt(4, signupUser.getBranchId());
			ps.setInt(5, signupUser.getPositionId());
			ps.setInt(6, signupUser.getIsStopped());
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}


	public User getUser(Connection connection, String loginId, String encPassword){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? and is_stopped = 0";
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, encPassword);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true){
				return null;
			}else if(userList.size() >= 2){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs)throws SQLException{
		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLoginId(rs.getString("login_id"));
				user.setEncPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setPositionId(rs.getInt("position_id"));
				user.setIsStopped(rs.getInt("is_stopped"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));
				ret.add(user);
			}
			return ret;
		}finally{
			CloseableUtil.close(rs);
		}
	}

	private List<User> toJoinUserList(ResultSet rs)throws SQLException{
		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()){
				User user = new User();
				user.setId(rs.getInt("users.id"));
				user.setLoginId(rs.getString("users.login_id"));
				user.setEncPassword(rs.getString("users.password"));
				user.setName(rs.getString("users.name"));
				user.setBranchId(rs.getInt("users.branch_id"));
				user.setBranchName(rs.getString("branches.name"));
				user.setPositionId(rs.getInt("users.position_id"));
				user.setPositionName(rs.getString("positions.name"));
				user.setIsStopped(rs.getInt("users.is_stopped"));
				user.setCreatedDate(rs.getTimestamp("users.created_date"));
				user.setUpdatedDate(rs.getTimestamp("users.updated_date"));
				ret.add(user);
			}
			return ret;
		}finally{
			CloseableUtil.close(rs);
		}
	}
}
