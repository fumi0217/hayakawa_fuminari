package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;
import utils.CloseableUtil;


public class CommentDao {
	public void deleteComment(Connection connection, int commentId){
		String sql = "delete from comments where id = ?";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, commentId);
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}

	public List<Comment> getComments(Connection connection){
		List<Comment> comments = new ArrayList<Comment>();
		Statement statement = null;
		ResultSet rs = null;
		String sql = "select * from comments inner join users on comments.user_id = users.id order by comments.created_date asc;";
		try{
			statement = connection.createStatement();
			rs = statement.executeQuery(sql);
			while(rs.next()){
				Comment comment = new Comment();
				comment.setId(rs.getInt("comments.id"));
				comment.setText(rs.getString("comments.text"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String createDate = sdf.format(rs.getTimestamp("comments.created_date"));
				comment.setCreatedDate(createDate);
				comment.setUserId(rs.getInt("comments.user_id"));
				comment.setMessageId(rs.getInt("comments.message_id"));
				comment.setUserName(rs.getString("users.name"));
				comment.setPositionId(rs.getInt("users.position_id"));
				comment.setBranchId(rs.getInt("users.branch_id"));
				comments.add(comment);
			}
			return comments;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(statement);
			CloseableUtil.close(rs);
		}
	}

	public int insert(Connection connection, Comment newComment){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("insert into comments(");
			sql.append("text,");
			sql.append("user_id,");
			sql.append("message_id");
			sql.append(")values(");
			sql.append("?,");
			sql.append("?,");
			sql.append("?)");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, newComment.getText());
			ps.setInt(2, newComment.getUserId());
			ps.setInt(3, newComment.getMessageId());

			return ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}
}
