package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.mysql.jdbc.Statement;

import beans.Message;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import utils.CloseableUtil;


public class MessageDao {
	public int countMessages(Connection connection, String category, String sDate, String eDate){
		String sql = "select count(*) as count from messages inner join users on messages.user_id = users.id"
				+ " where category like ? and messages.created_date between ? and ? order by messages.created_date desc";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String startDate;
		String endDate;
		if(StringUtils.isEmpty(category)){
			category = "";
		}
		if(StringUtils.isEmpty(sDate)){
			startDate = "2000-01-01 00:00:00";
		}else{
			startDate = sDate + " 00:00:00";
		}
		if(StringUtils.isEmpty(eDate)){
			endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}else{
			endDate = eDate + " 23:59:59";
		}
		int count = 0;
		try{
			Timestamp startingDate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate).getTime());
			Timestamp endingDate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate).getTime());
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%"+category+"%");
			ps.setTimestamp(2, startingDate);
			ps.setTimestamp(3, endingDate);
			rs = ps.executeQuery();
			while(rs.next()){
				count = rs.getInt("count");
			}
			return count;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			CloseableUtil.close(rs);
			CloseableUtil.close(ps);
		}
		return count;
	}
	public void deleteMessage(Connection connection, int messageId){
		String messageSQL = "delete from messages where id = ?";
		String commentSQL = "delete from comments where message_id = ?";
		PreparedStatement ps = null;
		try{
			ps = connection.prepareStatement(messageSQL);
			ps.setInt(1, messageId);
			ps.executeUpdate();
			ps = connection.prepareStatement(commentSQL);
			ps.setInt(1, messageId);
			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}

	public List<Message> sort(Connection connection, String category, String sDate, String eDate, int startLine) {
		String sql = "select * from messages inner join users on messages.user_id = users.id"
				+ " where category like ? and messages.created_date between ? and ? order by messages.created_date desc limit 20 offset " + startLine +";";
		ResultSet rs = null;
		PreparedStatement ps = null;
		List<Message> messageList = new ArrayList<Message>();
		String startDate;
		String endDate;
		if(StringUtils.isEmpty(category)){
			category = "";
		}
		if(StringUtils.isEmpty(sDate)){
			startDate = "2000-01-01 00:00:00";
		}else{
			startDate = sDate + " 00:00:00";
		}
		if(StringUtils.isEmpty(eDate)){
			endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}else{
			endDate = eDate + " 23:59:59";
		}
		try{
			Timestamp startingDate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDate).getTime());
			Timestamp endingDate = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate).getTime());
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%"+category+"%");
			ps.setTimestamp(2, startingDate);
			ps.setTimestamp(3, endingDate);
			rs = ps.executeQuery();
			messageList = toMessageList(rs);

			return messageList;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			CloseableUtil.close(ps);
			CloseableUtil.close(rs);
		}
		return messageList;

	}
	public List<Message> getAllMessages(Connection connection){
		String sql = "select * from messages inner join users on messages.user_id = users.id order by messages.created_date desc;";
		Statement statement = null;
		List<Message> messageList = new ArrayList<Message>();
		ResultSet rs = null;
		try{
			statement = (Statement)connection.createStatement();
			rs = statement.executeQuery(sql);
			messageList = toMessageList(rs);

			return messageList;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(statement);
			CloseableUtil.close(rs);
		}
	}

	public Timestamp getTime(Connection connection, int messageId){
		String sql = "select created_date from messages where id = ?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = connection.prepareStatement(sql);
			ps.setInt(1, messageId);
			rs = ps.executeQuery();
			rs.next();
			return rs.getTimestamp("created_date");
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
			CloseableUtil.close(rs);
		}
	}

	public void insert(Message newMessage, Connection connection){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("insert into messages(");
			sql.append("title,");
			sql.append("text,");
			sql.append("category,");
			sql.append("user_id");
			sql.append(")values(");
			sql.append("?,");
			sql.append("?,");
			sql.append("?,");
			sql.append("?)");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, newMessage.getTitle());
			ps.setString(2, newMessage.getText());
			ps.setString(3, newMessage.getCategory());
			ps.setInt(4, newMessage.getUserId());

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			CloseableUtil.close(ps);
		}
	}


	private List<Message> toMessageList(ResultSet rs)throws SQLException{
		List<Message> ret = new ArrayList<Message>();
		try{
			while(rs.next()){
				Message message = new Message();
				message.setId(rs.getInt("messages.id"));
				message.setTitle(rs.getString("messages.title"));
				message.setText(rs.getString("messages.text"));
				message.setCategory(rs.getString("messages.category"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				String createDate = sdf.format(rs.getTimestamp("messages.created_date"));
				message.setCreatedDate(createDate);
				message.setUserId(rs.getInt("messages.user_id"));
				message.setUserName(rs.getString("users.name"));
				message.setPositionId(rs.getInt("users.position_id"));
				message.setBranchId(rs.getInt("users.branch_id"));
				ret.add(message);
			}
			return ret;
		}finally{
			CloseableUtil.close(rs);
		}
	}

}
