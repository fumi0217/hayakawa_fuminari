
package service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import utils.CloseableUtil;
import utils.DBUtil;
import beans.Branch;
import beans.Position;
import dao.DownloadingData;

public class GettingDataService {
	public List<Branch> gettingBranches(){
		List<Branch> branches = new ArrayList<Branch>();
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			DownloadingData downloadingData = new DownloadingData();
			branches = downloadingData.gettingBranches(connection);
			return branches;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<Position> gettingPositions(){
		List<Position> positions = new ArrayList<Position>();
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			DownloadingData downloadingData = new DownloadingData();
			positions = downloadingData.gettingPositions(connection);
			return positions;
		}finally{
			CloseableUtil.close(connection);
		}
	}
}
