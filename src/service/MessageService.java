package service;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import beans.Message;
import dao.MessageDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class MessageService {
	public int countMessages(String category, String startDate, String endDate){

		Connection connection = null;

		try{
			connection = DBUtil.getConnection();

			MessageDao messageDao = new MessageDao();
			int count = messageDao.countMessages(connection, category, startDate, endDate);

			return count;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public void deleteMessage(int messageId){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			MessageDao messageDao = new MessageDao();
			messageDao.deleteMessage(connection, messageId);
			DBUtil.commit(connection);
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}
	public List<Message> sortMessages(String category, String startDate, String endDate, int startLine){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			MessageDao messageDao = new MessageDao();
			List<Message> messages = messageDao.sort(connection, category, startDate, endDate, startLine);
			return messages;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public Timestamp getTime(int messageId){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			MessageDao messageDao = new MessageDao();
			Timestamp created_date = messageDao.getTime(connection, messageId);
			return created_date;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<Message> getAllMessages(){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			MessageDao messageDao = new MessageDao();
			List<Message> messages = messageDao.getAllMessages(connection);
			return messages;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public void createNewMessage(Message newMessage){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			MessageDao messageDao = new MessageDao();
			messageDao.insert(newMessage, connection);

			DBUtil.commit(connection);

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}
}