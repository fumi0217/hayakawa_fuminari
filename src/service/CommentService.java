package service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import utils.CloseableUtil;
import utils.DBUtil;
import beans.Comment;
import dao.CommentDao;

public class CommentService {

	public void deleteComment(int commentId){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.deleteComment(connection, commentId);
			DBUtil.commit(connection);
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<Comment> getComments(){
		Connection connection = null;
		ArrayList<Comment> comments = new ArrayList<Comment>();
		try{
			connection = DBUtil.getConnection();
			CommentDao commentDao = new CommentDao();
			comments = (ArrayList<Comment>) commentDao.getComments(connection);
			return comments;

		}finally{
			CloseableUtil.close(connection);
		}
	}

	public boolean createNewComment(Comment newComment){
		int result;
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			CommentDao commentDao = new CommentDao();
			result = commentDao.insert(connection, newComment);
			DBUtil.commit(connection);

			if(result == 1){
				return true;
			}else{
				DBUtil.rollback(connection);
				return false;
			}
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

}