package service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;

public class UserService {

	public int countUsers(){

		Connection connection = null;

		try{
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			int count = userDao.countUsers(connection);

			return count;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public User getUser(String loginId){

		Connection connection = null;

		try{
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, loginId);

			return user;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<String> getLoginId(){
		Connection connection = null;
		List<String> loginId = new ArrayList<String>();
		try{
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			loginId = userDao.getLoginId(connection);
			return loginId;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public User login(String loginId, String password){

		Connection connection = null;

		try{
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, loginId, encPassword);

			DBUtil.commit(connection);

			return user;
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public void updateUser(User updatedUser){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			UserDao userDao = new UserDao();
			userDao.update(connection, updatedUser);
			DBUtil.commit(connection);
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public User getUserInformation(int userId){
		User edittedUser = new User();
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();
			UserDao userDao = new UserDao();
			edittedUser = userDao.getUserInformation(connection, userId);
			return edittedUser;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public void updateIsStopped(User isStoppedUser){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			userDao.updateIsStopped(connection, isStoppedUser);

			DBUtil.commit(connection);
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}


	public List<User> getAllUsers(String startLine, String column, String order){
		Connection connection = null;
		List<User> users = new ArrayList<User>();
		try{
			connection = DBUtil.getConnection();
			UserDao userDao = new UserDao();
			users = userDao.getAllUsers(connection, startLine, column, order);
			return users;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public void signup(User signupUser){

		Connection connection = null;

		try{
			connection = DBUtil.getConnection();
			UserDao userDao = new UserDao();
			userDao.insert(signupUser, connection);

			DBUtil.commit(connection);
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Error e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}
}
