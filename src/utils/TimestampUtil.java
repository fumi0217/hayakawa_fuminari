package utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class TimestampUtil {
    public static String formattedTimestamp(Timestamp timestamp, String timeFormat) {
        return new SimpleDateFormat(timeFormat).format(timestamp);
    }
}
